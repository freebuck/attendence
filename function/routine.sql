-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2016 at 07:10 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online`
--

-- --------------------------------------------------------

--
-- Table structure for table `routine`
--

CREATE TABLE `routine` (
  `id` int(9) NOT NULL,
  `sub` varchar(15) DEFAULT NULL,
  `sub_code` varchar(15) DEFAULT NULL,
  `start_time` varchar(60) DEFAULT NULL,
  `end_time` varchar(60) DEFAULT NULL,
  `batch` varchar(60) DEFAULT NULL,
  `section` varchar(5) DEFAULT NULL,
  `teacher` varchar(60) DEFAULT NULL,
  `day` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `routine`
--

INSERT INTO `routine` (`id`, `sub`, `sub_code`, `start_time`, `end_time`, `batch`, `section`, `teacher`, `day`) VALUES
(1, 'Bangla', '2211', '09:15:00:PM', '09:17:00:PM', '38', 'A', 'Alok Kanti Sharma', 'Fri'),
(2, 'English', '2212', '10:00:00:PM', '10:30:00:PM', '38', 'A', 'Alok Kanti Sharma', 'Fri');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `routine`
--
ALTER TABLE `routine`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `routine`
--
ALTER TABLE `routine`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
