Features of the project
As soon as a teacher or a student joins the institution, he/she is requested to sign up to the online attendance management system. After this, admin is required to review and approve their account activation.
As their account gets approved, they can access to their account.

Users / Actors of System
1. Admin
2. Teacher
3. Student

 Admin // I Think Head of the department will admin
• Add Update Delete Subject to be studied.
• Approve/Disapprove teacher/student account.
• Add teacher/student account.
• View attendance of each student.
• Download attendance Report as PDF format
• Update Records
Teacher
• He is required to create account and then gets approved by the admin.
• View the list of students assigned to them.
• Count attendance after each lecture
• View attendance of each student assigned to them.
• Download Report of attendance in PDF Format
• Update Profile
Student
• He/she needs to create an account.
• Can view their attendance.
• Update Profile
• And give attendance on class time