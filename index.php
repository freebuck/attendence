
<?php 
include 'inc/header.php';
if(isset($_SESSION['logged']) != "logged"){
    header("Location: login.php");
}
?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<center>
				
				<?php
					date_default_timezone_set('asia/dhaka');
					$day =  date('D');
					$batch = $_SESSION['batch'];
					$sec = $_SESSION['sec'];
					$sql = "SELECT MIN(start_time) FROM routine WHERE batch = '$batch' AND section = '$sec' AND day = '$day'";
					$qur = $con->query($sql);
					$check = $qur->num_rows;
					if($check > 0){
						while ($res = $qur->fetch_array()) {
							$day_start = date("H:i:s");
							$class_end = date("H:i:s", strtotime($day_start)+3600);
							$start_time = date("H:i:s", strtotime($res['MIN(start_time)']));
							$re_time = date("H:i:s", strtotime($res['MIN(start_time)'])-3600);
							if($day_start >= "00:00:00" && $day_start <= $re_time ){
								$sql1 = "SELECT * FROM routine WHERE batch = '$batch' AND section = '$sec' AND day = '$day'";
								$qur1 = $con->query($sql1);
								while($res1 = $qur1->fetch_array()){
									echo '<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							                 <div class="db-wrapper">
							                <div class="db-pricing-eleven db-bk-color-three">
							                 <div class="price">
							                     '.$res1['sub'].'
							                    </div>
							                    <div class="type">
							                        '.$res1['teacher'].'
							                    </div>
							                    <ul>

							                        <li><i class="glyphicon glyphicon-print"></i>Room : '.$res1['room'].'</li>
							                        <li><i class="glyphicon glyphicon-time"></i>'.$res1['batch'].'('.$res1['section'].')'.'</li>
							                        <li><i class="glyphicon glyphicon-trash"></i>'.$res1['start_time'].'-'.$res1['end_time'].'</li>
							                    </ul>
							                    <div class="pricing-footer">

							                    </div>
							                </div>
							                     </div>
							            </div>';
								}
							} else if($day_start >= $re_time){
								$sql2 = "SELECT * FROM routine WHERE batch = '$batch' AND section = '$sec' AND day = '$day' AND end_time >= '$day_start' ORDER BY start_time ASC LIMIT 1";
								$qur2 = $con->query($sql2);
								$check1 = $qur2->num_rows;
								if($check1 > 0){
									while($res2 = $qur2->fetch_array()){
										echo '<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4"></div>';
										echo '<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
							                 <div class="db-wrapper">
							                <div class="db-pricing-eleven db-bk-color-three">
							                 <div class="price">
							                     '.$res2['sub'].'
							                    </div>
							                    <div class="type">
							                        '.$res2['teacher'].'
							                    </div>
							                    <ul>

							                        <li><i class="glyphicon glyphicon-print"></i>Room : '.$res2['room'].'</li>
							                        <li><i class="glyphicon glyphicon-time"></i>'.$res2['batch'].'('.$res2['section'].')'.'</li>
							                        <li><i class="glyphicon glyphicon-trash"></i>'.$res2['start_time'].'-'.$res2['end_time'].'</li>
							                    </ul>
							                    <div class="pricing-footer">

							                        <a href="#" class="btn db-button-color-square btn-lg"> Attendance</a>
							                    </div>
							                </div>
							                     </div>
							            </div>';
									}
								} else {
									echo '<div class="col-md-4">';
									echo '</div>';
									echo '<div class="alert alert-danger col-md-4">';
									 echo ' <strong>Warning!</strong> Indicates a warning that might need attention.';
									echo '</div>';
									echo '<div class="col-md-4">';
									echo '</div>';
								}
							}
						}
					} else {
						echo "Today You Have no Class";
					}
				?>
			</center>

    </div>
			
		</div>
	</div>
</div>

<?php 
include 'inc/footer.php';
?>
