<?php 
session_start();
ob_start();
include 'function/database.php';
include 'function/base.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <title>POLO-MAR</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
   <link href="<?php echo base; ?>/inc/css/bootstrap.css " rel="stylesheet">
    <!-- customize this file if needed -->
   <link href="<?php echo base; ?>/inc/css/main.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="wrapper">
    <div class="container">
        <header>
            <div class="row">
                <div class="navbar">
                    <div class="navbar-header">
                        <button class="navbar-toggle collapsed" data-target=".navbar-collapse" data-toggle="collapse" type="button"><span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a class="navbar-brand" href="#">POLOMAR</a>
                    </div>

                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav">
                        <?php if(isset($_SESSION['logged']) == "logged"){ ?>
                            <li>
                                <a href="#">About</a>
                            </li>

                            <li>
                                <a href="#">Blog</a>
                            </li>

                            <li>
                                <a href="#">Resume</a>
                            </li>

                            <li>
                                <a href="logout.php">logout</a>
                            </li>
                          <?php  }   else { ?>

                            <li>
                                <a href="login.php">login</a>
                            </li>

                            <li>
                                <a href="#">sign up</a>
                            </li>
    
                           <?php  } ?>


                        </ul>
                    </div><!-- /.nav-collapse -->
                </div><!-- /.navbar -->
            </div>
            <?php if(isset($_SESSION['logged']) == "logged"){ ?>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <h1 class="intro"><span>Hello</span><?php echo '&nbsp'.$_SESSION['user']; ?> <span>, Designer &amp; Coder </span></h1>
                </div>
            </div>
            <?php } ?>
        </header>

        <div class="row grid-list-wrapper no-gutter-space" id="shots"></div>
     </div>
</div>