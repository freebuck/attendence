<?php  ?>
	 <footer>
        <div class="container">
            <nav class="nav-footer">
                <ul>
                    <li>
                        <a href="#">About</a>
                    </li>

                    <li>
                        <a href="#">Blog</a>
                    </li>

                    <li>
                        <a href="#">Resume</a>
                    </li>

                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>

                                  <ul>
                                      <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                                      <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                      <li><a href="#"> <i class="fa fa-github"></i> </a> </li>  
                                      <li><a href="#"> <i class="fa fa-linkedin"></i> </a></li>
                                      <li><a href="#"> <i class="fa fa-envelope"></i> </a></li>
                                  </ul>

                <p class="credits text-center">&copy; All Rights Reserved 2015</p>
            </nav>
        </div>

    </footer>
</body>
</html>