<?php
include 'inc/header.php';
?>

<?php

if(isset($_SESSION['logged']) == "logged"){
    header("Location: index.php");
}
else {
    
}
?>

<div class="container" style="margin-top: 5%;">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-primary">
            <div class="panel-heading">Login</div>
            <div class="panel-body">
            
            <!-- Login Form -->
            <form role="form" action="" method="post">
            
            <!-- Username Field -->
                <div class="row">
                    <div class="form-group col-xs-12">
                    <label for="username"><span class="text-danger" style="margin-right:5px;">*</span>Student Id:</label>
                        <div class="input-group">
                            <?php if(isset($_COOKIE['username']) && isset($_COOKIE['password'])) { 
                                $username = $_COOKIE['username'];
                                $password = $_COOKIE['password'];
                            ?>
                                <input class="form-control" id="username" type="text" name="username" placeholder="Student Id" value="<?php echo $username; ?>" required/>
                            <?php } else { ?>
                                <input class="form-control" id="username" type="text" name="username" placeholder="Student Id" required/>
                            <?php } ?>
                            <span class="input-group-btn">
                                <label class="btn btn-primary"><span class="glyphicon glyphicon-user" aria-hidden="true"></label>
                            </span>
                            </span>
                        </div>
                    </div>
                </div>
                
                <!-- Content Field -->
                <div class="row">
                    <div class="form-group col-xs-12">
                        <label for="password"><span class="text-danger" style="margin-right:5px;">*</span>Password:</label>
                        <div class="input-group">
                            <?php if(isset($_COOKIE['username']) && isset($_COOKIE['password'])) { 
                                $username = $_COOKIE['username'];
                                $password = $_COOKIE['password'];
                            ?>
                                <input class="form-control" id="password" type="password" name="password" placeholder="Password" value="<?php echo $password; ?>" required/>
                            <?php } else { ?>
                                <input class="form-control" id="password" type="password" name="password" placeholder="Password" required/>
                            <?php } ?>
                            <span class="input-group-btn">
                                <label class="btn btn-primary"><span class="glyphicon glyphicon-lock" aria-hidden="true"></label>
                            </span>
                            </span>
                        </div>
                    </div>
                </div>

                <!-- Remember Me -->
                <div>
                    <label class="remember" for="remember">
                        <?php
                            if(isset($_COOKIE['remember'])) {
                                echo "<input type=\"checkbox\" id=\"remember\" name=\"remember\" checked=\"checked\">&nbsp;Remember me";
                            } else {
                                echo "<input type=\"checkbox\" id=\"remember\" name=\"remember\">&nbsp;Remember me";
                            }
                        ?>
                    </label>
                </div>
                
                <!-- Login Button -->
                <div class="row">
                    <div class="form-group col-xs-4">
                        <button class="btn btn-primary" type="submit" name="submit">Submit</button>
                    </div>
                </div>
                                <?php

                                    if(isset($_POST['submit']))
                                    {

                                        
                                        $username = $con->real_escape_string($_POST['username']);
                                        $password = $con->real_escape_string($_POST['password']);

                                        if (isset($_COOKIE['username']) && isset($_COOKIE['password'])) {

                                            $username = $_COOKIE['username'];
                                            $password = $_COOKIE['password'];

                                        } elseif (isset($_POST['username'])) {

                                            $username = $_POST['username'];
                                            $password = $_POST['password'];

                                        }

                                        if(empty($username) or empty($password)){
                                            echo "<span style='color:red;'>Filed must not be empty...!</span>";
                                        } else {

                                            $sql = "SELECT * FROM student WHERE std_id='$username' limit 1";
                                            $result = $con->query($sql);

                                            while($row = $result->fetch_array()){
                                                if($row['std_id'] == $username && $row['std_pass'] == $password){
                                                $_SESSION['user'] = $row['std_name'];
                                                $_SESSION['logged'] = "logged";
                                                $_SESSION['id'] = $row['std_id'];
                                                $_SESSION['sec'] = $row['std_sec'];
                                                $_SESSION['batch'] = $row['std_batch'];
                                                $_SESSION['logged'] = "logged" ;
                                                if($_POST['remember']) {
                                                    $month = time() + (60 * 60 * 24 * 7);
                                                    setcookie('remember', $_POST['username'], $month);
                                                    setcookie('username', $_POST['username'], $month);
                                                    setcookie('password', $row['std_pass'], $month);
                                                } elseif (!$_POST['remember'] && !$_COOKIE['remember']) {
                                                    $past = time() - 100;
                                                    if (isset($_COOKIE['remember'])) {
                                                        setcookie('remember', '', $past);
                                                    } elseif (isset($_COOKIE['username'])) {
                                                        setcookie('username', '', $past);
                                                    } elseif (isset($_COOKIE['password'])) {
                                                        setcookie('password', '', $past);
                                                    }
                                                }
                                                exit(header("Location:index.php"));
                                            }else{
                                                echo "<span style='color:red;'>Wrong Password or Email</span>";
                                                }
                                            }
                                        }
                                    }
                                ?>
                
            </form>
            <!-- End of Login Form -->
            
        </div>
    </div>
</div>
<?php include 'inc/footer.php'; ?>